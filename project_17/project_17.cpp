﻿#include <iostream>
#include <cmath>


class vector
{
private:
	double x;
	double y;
	double z;

public:
	vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	
	void showmodule()
	{
		std::cout << " vector =" << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	};

	void showVector()
	{
		std::cout << " x = " << x << " y = " << y << " z = " << z << '\n';
	};
};

int main()
{
	vector v(4, 3, 1);
	v.showVector();
	v.showmodule();
}